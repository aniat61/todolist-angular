import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { EmitterVisitorContext } from '@angular/compiler/src/output/abstract_emitter';
import { TasksService } from '../services/tasks.service';

@Component({
  selector: 'app-todo-task',
  templateUrl: './todo-task.component.html',
  styleUrls: ['./todo-task.component.css']
})
export class TodoTaskComponent implements OnInit {

  taskList = [];

  constructor(private tasksTaskService: TasksService) {
    this.tasksTaskService.getTaskListObs().subscribe((tasks) => {
      this.taskList = tasks;
    });
  }
  ngOnInit() {
  }
  removeTask(index: number) {
    this.tasksTaskService.removeTask(index);
  }
  done(index: number) {
    this.tasksTaskService.done(index);
  }


}
