import {
  Task
} from './../model/task';
import {
  TasksService
} from './../services/tasks.service';
import {
  Component,
  EventEmitter,
  OnInit,
  Output
} from '@angular/core';


@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css']
})
export class AddTaskComponent implements OnInit {
  newTask: string;
  category: string = 'Inne';
  color: string = 'secondary';
  constructor(private tasksTaskService: TasksService) {

  }

  ngOnInit() {

  }

  addTask() {
    const task: Task = {
      name: this.newTask,
      created: new Date(),
      category: this.category,
      color: this.color
    };
    this.tasksTaskService.addTask(task);
    this.newTask = '';
  }
  onSelectionChange(category, color) {
    this.category = category;
    this.color = color;
  }

}
