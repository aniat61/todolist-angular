import { TasksService } from './../services/tasks.service';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-done-task',
  templateUrl: './done-task.component.html',
  styleUrls: ['./done-task.component.css']
})
export class DoneTaskComponent implements OnInit {

  doneTask = [];


  constructor(private tasksService: TasksService) {
    this.tasksService.getTaskDoneListObs().subscribe(task => {
      this.doneTask = task;
    });
  }

  ngOnInit() {
  }
  notDone(index: number) {
    this.tasksService.notDone(index);
  }
  removeTaskDone(index: number) {
    this.tasksService.removeTaskDone(index);
  }

}
