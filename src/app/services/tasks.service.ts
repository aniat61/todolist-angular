import { Task } from './../model/task';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class TasksService {
    private taskList: Array<Task> = [];
    private taskDoneList: Array<Task> = [];

    private taskListObs = new BehaviorSubject<Array<Task>>([]);
    private taskDoneListObs = new BehaviorSubject<Array<Task>>([]);
    constructor() {
        this.taskList = [{ name: 'Sprzątanie kuwety', created: new Date(), category: 'Inne', color: 'secondary' },
        { name: 'Nauka Angulara', created: new Date(), category: 'Praca', color: 'warning' },
        { name: 'Prasowanie', created: new Date(), category: 'Dom', color: 'success' }];
        this.taskListObs.next(this.taskList);
    }
    addTask(task: Task) {

        this.taskList.push(task);
        this.taskListObs.next(this.taskList);

    }
    removeTask(index) {
        this.taskList.splice(index, index + 1);
        this.taskListObs.next(this.taskList);
    }
    removeTaskDone(index) {
        this.taskDoneList.splice(index, index + 1);
        this.taskDoneListObs.next(this.taskDoneList);
    }
    done(index) {
        this.taskDoneList.push(this.taskList[index]);
        this.removeTask(index);
        this.taskDoneListObs.next(this.taskDoneList);

    }
    notDone(index) {
        this.taskList.push(this.taskDoneList[index]);
        this.removeTaskDone(index);
        this.taskListObs.next(this.taskList);
    }
    getTaskListObs(): Observable<Array<Task>> {
        return this.taskListObs;

    }
    getTaskDoneListObs(): Observable<Array<Task>> {
        return this.taskDoneListObs;

    }
}